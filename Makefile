DOCKER_COMPOSE = docker-compose
PRETTY_CMD = docker-compose run pretty

all: build test

pull:
	$(DOCKER_COMPOSE) pull

push:
	$(DOCKER_COMPOSE) push

build:
	$(DOCKER_COMPOSE) build --parallel

test:
	$(PRETTY_CMD) fix
	$(PRETTY_CMD)
	git diff --exit-code
