# Tool information

<!-- Fill the required fields: -->

- Name:
- Web site:
- Supported files: <!-- e.g.: .php,.js,.txt... put "global" if the tool is file type agnostic. -->

## Recommended configuration

<!--
  Put details about how to properly configure this tool.
  Note: A popular standard usage is recommended for simplicity (e.g. PSR for PHP)
-->

## Usages

<!--
  With what project's configuration this tool would be helpful?
  Links and examples are appreciated.
-->

# Additional context

<!-- Put anything else useful here. -->

/label ~"type::feature"
/label ~"priority::low"
