# Rules details

<!--
  Explain which rules to add, remove or update and how.
-->

## Why this change is needed?

<!--
  Explain here with concrete cases why this feature is needed and which benefits it will give.
  Adding links to official documentation about convention and/or best practices may be required.
-->

/label ~"type::feature"
/label ~"priority::low"
