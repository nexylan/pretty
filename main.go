package main

import (
	"context"
	"fmt"
	"io"
	"io/ioutil"
	"os"

	"github.com/bmatcuk/doublestar"
	"github.com/docker/docker/api/types"
	"github.com/docker/docker/api/types/container"
	"github.com/docker/docker/api/types/mount"
	"github.com/docker/docker/client"
	"github.com/docker/docker/pkg/stdcopy"
	ignore "github.com/sabhiram/go-git-ignore"
	"github.com/thoas/go-funk"
)

// ImageTag Default image tag to use
var ImageTag string

func run(
	ctx context.Context,
	cli *client.Client,
	tool string,
	mode string,
	imageTag string,
) int {
	image := fmt.Sprintf(
		"%s/%s:%s",
		env("IMAGE_NAME", "registry.gitlab.com/nexylan/pretty"),
		tool,
		imageTag,
	)
	fmt.Print(" [pull]")
	reader, err := cli.ImagePull(
		ctx,
		fmt.Sprintf("%s", image),
		types.ImagePullOptions{},
	)
	if err != nil {
		fmt.Println(" [error] " + err.Error())
	} else {
		// We need to read the output to make it synchrone.
		io.Copy(ioutil.Discard, reader)
		fmt.Println(" [OK]")
	}

	cwd, err := os.Getwd()
	if err != nil {
		panic(err)
	}
	cachePath := fmt.Sprintf("%s/.cache/pretty", cwd)
	os.MkdirAll(cachePath, os.ModePerm)
	cnt, err := cli.ContainerCreate(
		ctx,
		&container.Config{
			Image: image,
			Env: []string{
				fmt.Sprintf("MODE=%s", mode),
			},
			WorkingDir: cwd,
		},
		&container.HostConfig{
			Mounts: []mount.Mount{
				{
					Type:   mount.TypeBind,
					Source: cwd,
					Target: cwd,
				},
				{
					Type:   mount.TypeBind,
					Source: cachePath,
					Target: "/cache",
				},
			},
		},
		nil,
		"",
	)
	if err != nil {
		panic(err)
	}

	if err := cli.ContainerStart(ctx, cnt.ID, types.ContainerStartOptions{}); err != nil {
		panic(err)
	}

	cwait, err := cli.ContainerWait(ctx, cnt.ID)
	if err != nil {
		panic(err)
	}

	out, err := cli.ContainerLogs(ctx, cnt.ID, types.ContainerLogsOptions{
		ShowStdout: true,
		ShowStderr: true,
	})
	if err != nil {
		panic(err)
	}
	stdcopy.StdCopy(os.Stdout, os.Stderr, out)

	if err := cli.ContainerRemove(
		ctx,
		cnt.ID,
		types.ContainerRemoveOptions{
			RemoveVolumes: true,
		},
	); err != nil {
		panic(err)
	}

	return int(cwait)
}

func env(key string, fallback string) string {
	if value, exists := os.LookupEnv(key); exists {
		return value
	}
	return fallback
}

func match(globs []string) bool {
	gitignore, _ := ignore.CompileIgnoreFile(".gitignore")

	for _, glob := range globs {
		matches, err := doublestar.Glob(
			fmt.Sprintf("**/%s", glob),
		)
		if err != nil {
			panic(err)
		}

		if len(funk.FilterString(matches, func(path string) bool {
			return nil == gitignore || !gitignore.MatchesPath(path)
		})) > 0 {
			return true
		}
	}

	return len(globs) == 0
}

func main() {
	cwd, _ := os.Getwd()
	fmt.Printf("Path: %s\n", cwd)
	mode := "lint"
	if len(os.Args[1:]) > 0 {
		mode = os.Args[1]
	}
	if mode != "lint" && mode != "fix" {
		fmt.Fprintf(os.Stderr, "Undefined mode: %s\n", mode)
		fmt.Fprintln(os.Stderr, "Available modes: lint, fix")
		os.Exit(2)
	}
	fmt.Printf("Mode: %s\n", mode)

	imageTag := "latest"
	if ImageTag != "" {
		imageTag = ImageTag
	}
	imageTag = env("IMAGE_TAG", imageTag)
	fmt.Printf("Tag: %s\n", imageTag)

	ctx := context.Background()
	cli, err := client.NewEnvClient()
	if err != nil {
		panic(err)
	}

	ret := 0
	for _, tool := range tools() {
		fmt.Printf("Tool: %s", tool.name)
		if !match(tool.globs) {
			fmt.Println(" [skip]")
			continue
		}
		ret += run(ctx, cli, tool.name, mode, imageTag)
	}

	if ret != 0 {
		fmt.Println()
		fmt.Println("It looks like some files need addressing.")
		fmt.Println("Please run the 'pretty fix' command to solve the issue.")
		fmt.Println("More info at: https://gitlab.com/nexylan/pretty")
	}

	os.Exit(ret)
}
