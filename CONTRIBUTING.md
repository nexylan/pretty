# Contributing

## Adding a tool

Let assume you want to add the tool named "awesome".

1. 🆕 Create a directory for the tool: `mkdir tools/awesome`. All related files must be here.
2. 🐳 Create a `Dockerfile`. A `CMD` running the tool must be defined.
3. 📝 Add your `awesome` tool to the list under `tools.go` and `docker-compose.yml`.
4. 🐰 Add some sample files related to your tool under the `fixtures` folder.
5. 📘 Add some documentation about the tool on the `README.md` file.
6. 💪 Submit your merge request with your new `awesome` tool!

🚨 Please be aware of some conditions to feel while adding your tool:

- It should handle the `lint` and `fix` via the `MODE` env var.
  If the tool can not fix files, just skip the process.
- Managed extensions of the file should be defined under `tools.go` for performance reasons.
- Ignored files from `.gitignore` should be managed. Search if your tool has an option for it or do it manually.

Quite simple, isn't it?

Not sure of what you have to do? Take a look at the already defined tools!

Still not sure? Please take a moment to [ask us your questions][issues].

[issues]: https://gitlab.com/nexylan/pretty/issues "Issues list"
