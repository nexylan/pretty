package main

type tool struct {
	name string

	// Required file extensions to run the tool.
	globs []string
}

// Tools are declared here.
// Extensions are use to avoid useless tool run.
func tools() []tool {
	return []tool{
		{
			name: "eslint",
			globs: []string{
				"*.js",
				"*.ts",
				// ReactJS
				"*.jsx",
				"*.tsx",
				// VueJS
				"*.vue",
			},
		},
		{
			name: "gofmt",
			globs: []string{
				"*.go",
			},
		},
		{
			name: "golint",
			globs: []string{
				"*.go",
			},
		},
		{
			name: "go-vet",
			globs: []string{
				"*.go",
			},
		},
		{
			name: "hadolint",
			globs: []string{
				"Dockerfile",
				"*.dockerfile",
			},
		},
		{
			name: "phpcs",
			globs: []string{
				"*.php",
				"*.php_cs",
			},
		},
		{
			name: "php-cs-fixer",
			globs: []string{
				"*.php",
				"*.php_cs",
			},
		},
		{
			name: "prettier",
			globs: []string{
				"*.md",
				"*.json",
				"*.yml",
				"*.yaml",
				"*.pug",
				"*.graphql",
			},
		},
		{
			name: "rubocop",
			globs: []string{
				"*.rb",
			},
		},
		{
			name: "shellcheck",
			globs: []string{
				"*.sh",
			},
		},
		{
			name: "shfmt",
		},
		{
			name: "stylelint",
			globs: []string{
				"*.css",
				"*.scss",
			},
		},
	}
}
