module nexylan/pretty

go 1.12

require (
	github.com/Microsoft/go-winio v0.4.14 // indirect
	github.com/bmatcuk/doublestar v1.1.5
	github.com/docker/distribution v2.7.1+incompatible // indirect
	github.com/docker/docker v1.13.1
	github.com/docker/go-connections v0.4.0 // indirect
	github.com/docker/go-units v0.4.0 // indirect
	github.com/opencontainers/go-digest v1.0.0-rc1 // indirect
	github.com/sabhiram/go-git-ignore v0.0.0-20180611051255-d3107576ba94
	github.com/thoas/go-funk v0.4.0
	golang.org/x/net v0.0.0-20190909003024-a7b16738d86b // indirect
)
