# Pretty

Your coding style companion. See how cute it is:

![Puppy](https://media.giphy.com/media/Xev2JdopBxGj1LuGvt/giphy.gif)

## Requirements

- [Docker][docker_get_started]

## Installation

To simplify the cli usage, you might want to setup an alias:

```bashrc
alias pretty='docker run --rm \
  --env IMAGE_TAG \
  --volume "${PWD}":"${PWD}" \
  --volume /var/run/docker.sock:/var/run/docker.sock \
  --workdir ${PWD} \
  registry.gitlab.com/nexylan/pretty:x.y.z \
'
```

Replace `x.y.z` by the latest available release number:
https://gitlab.com/nexylan/pretty/-/releases

## Usage

Am I pretty?

```
pretty
// Wild ugly style errors appear!
```

![omg](/uploads/ea2eb743a04f618523c4b94e408335e4/cristi.png)

ℹ A `.cache` directory will be created on each run. You may add it on your ignore rules.

Oh là là ! Let's me fix that.

```
pretty fix
// Fancy log output.
```

What do you think Cristina?

![better](/uploads/5b9e4fbce454f9904d419e300b771c4b/jadore.gif)

Yes, I know.

To run a specific tag bith the binary:

```
IMAGE_TAG=1.2 pretty
```

### GitLab CI usage

```yaml
lint:
  stage: test
  image: docker:19
  services:
    - docker:19-dind
  only:
    - master
    - merge_requests
  script: docker run --rm
    --volume "${PWD}":"${PWD}"
    --volume /var/run/docker.sock:/var/run/docker.sock
    --workdir "${PWD}"
    registry.gitlab.com/nexylan/pretty:x.y.z
```

Nothing more.

### Dev mode

```
make
```

## Implemented tools

We are trying to trust the most popular standard to avoid
loosing time making our own.

Feel free to make any request or contributions to add or remove a rule,
with good arguments! 😉

### ESLint

The pluggable linting utility for JavaScript and JSX.

We are using the most popular AirBNB standard alongside the recommended ESLint rules.

🌐 https://eslint.org/

Followed standards:

- [AirBNB][airbnb-standard]
- [VueJS][vuejs-standard]
- [custom](tools/eslint/.eslintrc.js)

#### JS framework components rules guidelines

- Blocks order: Script, Style, Template (Svelte style)

### gofmt

Golang file linter.

🌐 https://golang.org/cmd/gofmt/

### golint

Golint is a linter for Go source code.

🌐 https://github.com/golang/lint

### go vet

Golang potential error scanner.

🌐 https://golang.org/cmd/vet/

### Hadolint

Dockerfile linter, validate inline bash, written in Haskell.

🌐 https://github.com/hadolint/hadolint

### PHP Coding Standards Fixer

Formerly named PHP CS Fixer or php-cs-fixer.

🌐 https://cs.symfony.com/

Followed standards:

- [PSR + custom rules](tools/php-cs-fixer/rootfs/.php_cs)

### Prettier

Opinionated code formatter. Supports many languages.

⚠️ Prettier is not used against JS files because of ESLint incompatibilities.
⚠️ If a language dedicated tool cans do more tha Prettier, it should be prefered.

🌐 https://prettier.io/

### Rubocop

The most popular Ruby linter.

🌐 https://docs.rubocop.org

### Shfmt

Shell script linter.

🌐 https://github.com/mvdan/sh#shfmt

### Stylelint

CSS and SCSS linter.

🌐 https://stylelint.io/user-guide/cli

Followed standards:

- [stylelint-config-standard][stylelint-config-standard]
- [stylelint-config-recommended-scss][stylelint-config-recommended-scss]

[docker_get_started]: https://www.docker.com/get-started "Get started."
[airbnb-standard]: https://github.com/airbnb/javascript#table-of-contents "Rules list"
[vuejs-standard]: https://eslint.vuejs.org/rules "Rules list"
[stylelint-config-standard]: https://stylelint.io/user-guide/rules "Rules list"
[stylelint-config-recommended-scss]: https://github.com/stylelint/stylelint/blob/master/docs/user-guide/rules.md#list-of-rules "Rules list"
