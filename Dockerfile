FROM docker:19-git AS dev
RUN apk add --no-cache make=~4 docker-compose=~1

FROM golang:1.13-alpine AS build
RUN apk add --no-cache git=~2
WORKDIR /code
COPY . .
ARG IMAGE_TAG
RUN CGO_ENABLED=0 GOOS=linux \
  go build \
  -ldflags "-X main.ImageTag=${IMAGE_TAG:-latest}" \
  -o pretty

FROM scratch
COPY --from=build /code/pretty /bin/pretty
ENTRYPOINT ["pretty"]
