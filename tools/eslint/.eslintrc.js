// Import AirBNB base rules to wrap them onto framework specific configs.
const airbnbRulesStyle = require('eslint-config-airbnb-base/rules/style').rules;
const airbnbRulesES6 = require('eslint-config-airbnb-base/rules/es6').rules;
const airbnbRulesBestPractices = require('eslint-config-airbnb-base/rules/best-practices').rules;

const rules = {
  'array-element-newline': ['error', 'consistent'],
  'array-bracket-newline': ['error', 'consistent'],
  'max-len': ['error', {
    code: 120,
    ignoreUrls: true,
  }],
  'no-param-reassign': ['error', {
    props: false,
  }],
  'no-plusplus': ['error', {
    allowForLoopAfterthoughts: true,
  }],
  'no-spaced-func': 'off',
  'no-underscore-dangle': ['error', {
    allow: ['_id'],
  }],
  'import/extensions': ['error', 'always', {
    ignorePackages: true,
    pattern: {
      js: 'never',
      jsx: 'never',
      ts: 'never',
      tsx: 'never',
    },
  }],
  'import/no-extraneous-dependencies': 'off',
  'import/no-unresolved': 'off',
  'object-property-newline': ['error', {
    allowAllPropertiesOnSameLine: false,
  }],
};

const tsRules = {
  '@typescript-eslint/explicit-function-return-type': ['error', {
    allowExpressions: true,
  }],
  '@typescript-eslint/no-unused-vars': ['error', {
    args: 'none',
  }],
  '@typescript-eslint/no-explicit-any': 'off',
  // @see https://github.com/typescript-eslint/typescript-eslint/issues/2483
  'no-shadow': 'off',
  '@typescript-eslint/no-shadow': 'error',
  // @see https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/func-call-spacing.md
  'func-call-spacing': 'off',
  '@typescript-eslint/func-call-spacing': ['error'],
};

const vueRules = {
  indent: 'off', // @see https://eslint.vuejs.org/rules/script-indent.html#options
  'vue/array-bracket-spacing': airbnbRulesStyle['array-bracket-spacing'],
  'vue/arrow-spacing': airbnbRulesES6['arrow-spacing'],
  'vue/block-spacing': airbnbRulesStyle['block-spacing'],
  'vue/brace-style': airbnbRulesStyle['brace-style'],
  'vue/camelcase': airbnbRulesStyle.camelcase,
  'vue/comma-dangle': airbnbRulesStyle['comma-dangle'],
  'vue/component-definition-name-casing': 'error',
  'vue/component-name-in-template-casing': 'error',
  'vue/component-tags-order': ['error', {
    order: ['script', 'style', 'template'],
  }],
  'vue/dot-location': airbnbRulesBestPractices['dot-location'],
  'vue/eqeqeq': airbnbRulesBestPractices.eqeqeq,
  'vue/key-spacing': airbnbRulesStyle['key-spacing'],
  'vue/keyword-spacing': airbnbRulesStyle['keyword-spacing'],
  'vue/match-component-file-name': ['error', {
    extensions: ['vue'],
    shouldMatchCase: true,
  }],
  'vue/no-empty-pattern': airbnbRulesBestPractices['no-empty-pattern'],
  'vue/no-irregular-whitespace': 'error',
  'vue/no-reserved-component-names': 'error',
  'vue/no-restricted-syntax': airbnbRulesStyle['no-restricted-syntax'],
  'vue/object-curly-spacing': airbnbRulesStyle['object-curly-spacing'],
  'vue/padding-line-between-blocks': 'error',
  'vue/script-indent': ['error', 2, {
    baseIndent: 1,
  }],
  'vue/space-infix-ops': airbnbRulesStyle['space-infix-ops'],
  'vue/space-unary-ops': airbnbRulesStyle['space-unary-ops'],
  'vue/v-on-function-call': 'error',
  'vue/v-slot-style': 'error',
  'vue/valid-v-slot': 'error',
};

module.exports = {
  env: {
    browser: true,
    commonjs: true,
    es6: true,
    node: true,
    jquery: true,
    'cypress/globals': true,
  },
  plugins: [
    'cypress',
    '@typescript-eslint',
  ],
  extends: [
    'eslint:recommended',
    'airbnb',
  ],
  parser: '@typescript-eslint/parser',
  parserOptions: {
    ecmaVersion: 2018,
    sourceType: 'module',
  },
  rules,
  overrides: [
    {
      files: ['*.ts', '*.tsx'],
      extends: [
        'plugin:@typescript-eslint/eslint-recommended',
        'plugin:@typescript-eslint/recommended',
      ],
      rules: tsRules,
    },
    {
      files: ['*.vue'],
      extends: [
        'plugin:vue/recommended',
      ],
      rules: vueRules,
    },
    {
      files: ['*.jsx', '*.tsx'],
      extends: [
        'plugin:react/recommended',
      ],
      parser: '@typescript-eslint/parser',
      rules: {
        'react/jsx-filename-extension': ['error', {
          extensions: ['.jsx', '.tsx'],
        }],
        'react/jsx-props-no-spreading': 'off',
        'react/require-default-props': 'off',
        // @see https://stackoverflow.com/a/63862578/1731473
        // @see https://github.com/typescript-eslint/typescript-eslint/issues/2540#issuecomment-692866111
        'no-use-before-define': 'off',
        '@typescript-eslint/no-use-before-define': 'error',
        // @see https://github.com/yannickcr/eslint-plugin-react/issues/2777
        'react/prop-types': 'off',
      },
    },
  ],
  settings: {
    'import/resolver': {
      node: {
        extensions: [
          '.js',
          '.jsx',
          '.ts',
          '.d.ts',
          '.tsx',
        ],
      },
    },
  },
};
