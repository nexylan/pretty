<?php

$finder = PhpCsFixer\Finder::create()
    ->ignoreVCS(true)
    ->ignoreVCSIgnored(true)
;

$rules = [
    '@PSR1' => true,
    '@PSR2' => true,
    '@PHP56Migration' => true,
    '@PHP56Migration:risky' => true,
    '@PHP70Migration' => true,
    '@PHP70Migration:risky' => true,
    '@PHP71Migration' => true,
    '@PHP71Migration:risky' => true,
    '@PHPUnit75Migration:risky' => true,
    '@DoctrineAnnotation' => true,
    '@PhpCsFixer' => true,
    '@PhpCsFixer:risky' => true,
    '@Symfony' => true,
    '@Symfony:risky' => true,
    'array_syntax' => [
        'syntax' => 'short'
    ],
    'comment_to_phpdoc' => false,
    'doctrine_annotation_indentation' => [
        'indent_mixed_lines' => true,
    ],
    'general_phpdoc_annotation_remove' => [
        'author',
        'covers',
    ],
    'linebreak_after_opening_tag' => true,
    'mb_str_functions' => true,
    'method_chaining_indentation' => false,
    'multiline_whitespace_before_semicolons' => [
        'strategy' => 'new_line_for_chained_calls',
    ],
    'native_function_invocation' => true,
    'no_php4_constructor' => true,
    // @see https://github.com/FriendsOfPHP/PHP-CS-Fixer/issues/5505
    'phpdoc_to_comment' => false,
    'php_unit_internal_class' => false,
    'php_unit_strict' => false,
    'php_unit_test_class_requires_covers' => false,
    'php_unit_test_case_static_method_calls' => [
        // Matches phpcs SlevomatCodingStandard.Classes.UselessLateStaticBinding rule.
        'call_type' => 'self',
    ],
    'phpdoc_summary' => false,
    'single_line_throw' => false,
    'static_lambda' => true,
    'yoda_style' => [
        'equal' => false,
        'identical' => false,
    ],
];

return PhpCsFixer\Config::create()
    ->setRules($rules)
    ->setFinder($finder)
;
