import React, { ReactElement, FunctionComponent } from 'react';
import { StyleSheet, Text, View } from 'react-native';

interface Props {
  name: string;
  optional?: boolean;
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

const Hello: FunctionComponent<Props> = ({ name, optional }) => (
  <View>
    <Text>
      Hello
      {name}
      !
      {optional ? 'This is great' : 'Never mind'}
    </Text>
  </View>
);

type GenericComponentProps<Item extends any> = {
  foo: Item;
}

export const GenericComponent = <Item extends any>({
  foo,
}: GenericComponentProps<Item>): ReactElement => (
  <div>{foo}</div>
  );

type GenericType<T> = {
  foo: T;
}

export type GenericTestType = GenericType<typeof GenericComponent<number>>;

export default function App(props: Props): ReactElement {
  return (
    <View style={styles.container}>
      <Text>Open up App.tsx to start working on your app!</Text>
      <Hello {...props} />
    </View>
  );
}
