let count = 1;
if (count < 2) {
  count += 1;
}

const singleProp = { prop: 'foo' };
singleProp.second = null;

const object = {
  _id: 42,
  foo: 'bar',
  number: 42,
};
// eslint-disable-next-line no-console
console.log(object._id);
object.thing = 'awesome';

const test = [
  'foo',
  'bar',
];
test.push('event');

function foo(data) {
  data.test = 42;
}
foo({});

for (let i = 0; i < 10; i++) {
  // Looping stuff.
}
