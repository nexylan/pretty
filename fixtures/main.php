<?php

declare(strict_types=1);

/**
 * @return iterable<string, array<array<string, int>, boolean>>
 */
function returnIterable(): iterable
{
    yield 'foo' => [
        [
            'bar' => 1,
            'thing' => 2020,
        ],
        false,
    ];
}

class User
{
    public const DEFAULT_USERNAME = 'blabla';

    public string $username;

    public string $firstName;

    public function __construct(string $username = '', string $firstName = '')
    {
        $this->username = $username;
        $this->firstName = $firstName;
    }

    public function getUsername(): string
    {
        return $this->username;
    }

    public function getFirstName(): string
    {
        return $this->firstName;
    }

    public function sayHello(?string $name = null): void
    {
        echo "Hello {$name}!";
    }
}

$user = new User();

if ($user->username === 'jdoe') {
    echo 'Hello, John.';
}

if ($this->getUsername() === 'test') {
    echo 'Test env';
}

throw new \Exception(
    'This is on a new line.'
);

// This is a wonderful comment.
// This is a comment.
//
// With an another line.
$names = [
    'toto',
    'tata',
    'tutu',
];
$prefixedNames = \array_map(
    static fn (string $name): string => 'test_'.$name,
    $names
);
foreach ($prefixedNames as $prefixedName) {
    echo $prefixedName."\n";
}
