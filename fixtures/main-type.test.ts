interface Data {
  foo: string;
}

// @ts-expect-error needs jest import.
test.each<[string, () => Partial<Data>, any]>([
  [
    'empty data', () => ({}), {},
  ],
])();
