# frozen_string_literal: true

def bad_name
  test if something
end

def say_hello(first, last)
  var = 'Hello, ' + first + ' ' + last
  var
end

puts say_hello(
  first,
  last,
)

array = [
  1,
  2,
]

puts array

hash = {
  this: 'is',
  an: 'hash',
}

puts hash
