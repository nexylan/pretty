import './main-type';
import { Bar } from 'bar';

enum Direction {
  Up = 1,
  Down = -1
}
interface Thing {
  foo: Bar;
  dir: Direction;
}

const some: Thing = {
  foo: new Bar(),
  dir: Direction.Up,
};
// eslint-disable-next-line no-console
console.log(some);

let count = 1;
if (count < 2) {
  count += 1;
}

const singleProp = { prop: 'foo' };
singleProp.second = null;

const object = {
  foo: 'bar',
  number: 42,
};
object.thing = 'awesome';

const test = [
  'foo',
  'bar',
];
test.push('event');
